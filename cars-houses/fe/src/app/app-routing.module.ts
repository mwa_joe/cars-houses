import { FrontDeskModule } from './front-desk/front-desk.module';
import { AdminModule } from './admin/admin.module';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './front-desk/home/home.component';

const routes: Routes = [
  {path: "", component: HomeComponent},
  {path: "home", loadChildren: () => FrontDeskModule},
  {path: "admin", loadChildren: () => AdminModule}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
