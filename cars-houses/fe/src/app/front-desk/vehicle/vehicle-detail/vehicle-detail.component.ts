import { VehiclesService } from './../../../services/vehicles.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-vehicle-detail',
  templateUrl: './vehicle-detail.component.html',
  styleUrls: ['./vehicle-detail.component.styl']
})
export class VehicleDetailComponent implements OnInit {
  id = undefined;
  vehicle = undefined;

  constructor(private vehicleService: VehiclesService, private route: ActivatedRoute) { }
  
  async ngOnInit() {

    this.id = this.route.snapshot.paramMap.get("id");
    this.vehicle = await this.vehicleService.getVehicleById(this.id).subscribe((response) => {
      this.vehicle = response.json()[0]; 
    });

  }
}
