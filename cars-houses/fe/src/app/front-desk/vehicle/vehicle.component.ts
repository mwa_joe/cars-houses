import { Component, OnInit } from '@angular/core';

import { VehiclesService } from '../../services/vehicles.service';
import { Router } from '@angular/router';

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'}
];

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.styl']
})
export class VehicleComponent implements OnInit {

  displayedColumns: string[] = ['make', 'year', 'mileage', 'color', 'price'];
  dataSource = ELEMENT_DATA;

  // initialize
  make = [];
  year = [];
  price = [];
  mileage = [];
  vehicles = [];
  vehicleModels = [];
  vehicleMakes = [];
  fuelTypes = ["Petrol", "Diesel", "Hybrid", "Electric"];
  bodyType = ["convertible", "coupe", "crossover", "hatchback", "mini", "pickup", "sedan", "suv", "van", "wagon"];
  transmission = ["Automatic", "Manual"];

  constructor( private vehicleService: VehiclesService, private router: Router) { }

  selCard = function (i){
    this.router.navigate(['/vehicle', i._id]);
  }

  ngOnInit() {

    // get the year range
    var d = new Date();
    var year_end = d.getFullYear();
    dateRange(1970, year_end, this.year);

    // get the price range
    range(100000, 10000000, 100000, this.price);

    // get the mileage range
    range(0, 400000, 10000, this.mileage);

    // range with step
    function range(start, stop, step, arr){
      if (typeof stop=='undefined'){
          // one param defined
          stop = start;
          start = 0;
      };
      if (typeof step=='undefined'){
          step = 1;
      };
      for (var i=start; step>0 ? i<=stop : i>stop; i+=step){
          arr.push(i);
      };
    };

    // date range
    function dateRange(min, max, arr){
      while (min < max + 1) {
        arr.push(min++);
      }
    }

    // return vehicle models
    this.vehicleService.getVehicleModels().subscribe((response) => {
      this.vehicleModels = response.json();
    });

    // return vehicle makes
    this.vehicleService.getVehicleMakes().subscribe((response) => {
      this.vehicleMakes = response.json();
    });

    // return all vehicles
    this.vehicleService.getVehicles().subscribe((response) => {
      this.vehicles = response.json();
    });
  }

}
