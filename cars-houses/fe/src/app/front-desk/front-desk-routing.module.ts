import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { MachineryComponent } from './machinery/machinery.component';
import { PropertyComponent } from './property/property.component';
import { VehicleDetailComponent } from './vehicle/vehicle-detail/vehicle-detail.component';

const routes: Routes = [
  { path: "", component: HomeComponent},
  { path: 'vehicles', component: VehicleComponent },
  { path: "vehicle/:id", component: VehicleDetailComponent},
  { path: "machinery", component: MachineryComponent},
  { path: "properties", component: PropertyComponent}
];

export const FrontdeskRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
