import { HttpModule } from '@angular/http';
import { FrontdeskRoutingModule } from './front-desk-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatTableModule} from '@angular/material/table';

import { UiModule } from '../ui/ui.module';

import { HomeComponent } from './home/home.component';

import { VehicleComponent } from './vehicle/vehicle.component';

import { PropertyComponent } from './property/property.component';
import { MachineryComponent } from './machinery/machinery.component';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDividerModule} from '@angular/material/divider';

import { WrapperComponent } from './wrapper/wrapper.component';
import { VehicleDetailComponent } from './vehicle/vehicle-detail/vehicle-detail.component';


export class AppRoutingModule {}


@NgModule({
  declarations: [HomeComponent, VehicleComponent, PropertyComponent, MachineryComponent, WrapperComponent, VehicleDetailComponent],
  imports: [
    MatTableModule,
    FrontdeskRoutingModule,
    HttpModule,
    UiModule,
    CommonModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatExpansionModule,
    MatDividerModule
  ]
})
export class FrontDeskModule { }
