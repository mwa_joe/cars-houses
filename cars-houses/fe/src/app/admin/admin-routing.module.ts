import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminComponent } from './admin/admin.component';
import { MachineryAdminComponent } from './machinery-admin/machinery-admin.component';
import { PropertyAdminComponent } from './property-admin/property-admin.component';
import { VehicleAdminComponent } from './vehicle-admin/vehicle-admin.component';

const routes: Routes = [
  {path: "", component: AdminComponent},
  {path: "vehicles", component: VehicleAdminComponent},
  {path: "property", component: PropertyAdminComponent},
  {path: "machinery", component: MachineryAdminComponent}
];

export const AdminRoutingModule: ModuleWithProviders = RouterModule.forChild(routes);
