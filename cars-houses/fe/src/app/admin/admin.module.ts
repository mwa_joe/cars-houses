import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin/admin.component';
import { AdminRoutingModule } from './admin-routing.module';
import { PropertyAdminComponent } from './property-admin/property-admin.component';
import { MachineryAdminComponent } from './machinery-admin/machinery-admin.component';
import { VehicleAdminComponent } from './vehicle-admin/vehicle-admin.component';
import { UiModule } from '../ui/ui.module';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatCardModule} from '@angular/material/card';
import {MatSelectModule} from '@angular/material/select';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [AdminComponent, VehicleAdminComponent, PropertyAdminComponent, MachineryAdminComponent, VehicleAdminComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    UiModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule
  ]
})
export class AdminModule { }
