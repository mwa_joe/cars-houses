import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, FormGroup, Validators} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { VehiclesService } from 'src/app/services/vehicles.service';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-vehicle-admin',
  templateUrl: './vehicle-admin.component.html',
  styleUrls: ['./vehicle-admin.component.styl']
})
export class VehicleAdminComponent implements OnInit {

  fuelTypes = ["Petrol", "Diesel", "Hybrid", "Electric"];
  bodyType = ["Convertible", "Coupe", "Crossover", "Hatchback", "Mini", "Pickup", "Sedan", "Suv", "Van", "Wagon"];
  transmission = ["Automatic", "Manual"]; 

  image:File = undefined;

  constructor(private vehicleService: VehiclesService) { }

  ngOnInit() {
  }

  onImageUpload(event){
    this.image = <File>event.target.files[0];
    console.log("")
  }

  uploadImage(){
    const fd = new FormData();
    fd.append('image', this.image, this.image.name);
    this.vehicleService.uploadImage(fd).subscribe((response) => {
      console.log("File: " + fd);
    });    
  }

  newVehicleFormGroup = new FormGroup({
    make: new FormControl('', Validators.required),
    model: new FormControl('', Validators.required),
    bodyType: new FormControl('', Validators.required),
    year: new FormControl('', Validators.required),
    mileage: new FormControl('', Validators.required),
    engine: new FormControl('', Validators.required),
    price: new FormControl('', Validators.required),
    transmission: new FormControl('', Validators.required),
    fuel: new FormControl('', Validators.required),
    condition: new FormControl('', Validators.required),
    color: new FormControl('', Validators.required)
  });

  createVehicle(){

    let vehicle = {
      "make": (this.newVehicleFormGroup.value.make).toLowerCase(),
      "model": (this.newVehicleFormGroup.value.model).toLowerCase(),
      "bodyType": (this.newVehicleFormGroup.value.bodyType).toLowerCase(),
      "year": (this.newVehicleFormGroup.value.year).toLowerCase(),
      "mileage": (this.newVehicleFormGroup.value.mileage).toLowerCase(),
      "engineSize": (this.newVehicleFormGroup.value.engine).toLowerCase(),
      "price": (this.newVehicleFormGroup.value.price).toLowerCase(),
      "transmission": (this.newVehicleFormGroup.value.transmission).toLowerCase(),
      "condition": (this.newVehicleFormGroup.value.condition).toLowerCase(),
      "fuelType": (this.newVehicleFormGroup.value.fuel).toLowerCase(),
      "color": (this.newVehicleFormGroup.value.color).toLowerCase()
    }

     this.vehicleService.postVehicle(vehicle).subscribe((response) => {
       console.log(response);
     });
  }

}
