import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FrontDeskModule } from './front-desk/front-desk.module';
import { AdminModule } from './admin/admin.module';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { UiModule } from './ui/ui.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FrontDeskModule,
    AdminModule,
    NgbModule,
    UiModule
  ],
  exports: [NgbModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
