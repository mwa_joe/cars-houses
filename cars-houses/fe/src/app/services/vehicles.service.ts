import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, of } from 'rxjs';

let vehicles: [];
let vehicleModels: [];

@Injectable({
  providedIn: 'root'
})
export class VehiclesService {

  constructor( private http: Http) {}

  baseUri = 'http://localhost:3000';
  
  getVehicles(){
    return this.http.get(this.baseUri + "/vehicles");
  }

  getVehicleById(id){
    return this.http.get((this.baseUri + "/vehicles/"), {
      params: {id: id}
    });
  }

  getVehicleModels(){
    return this.http.get(this.baseUri + "/vehicles/models");
  }

  getVehicleMakes(){
    return this.http.get(this.baseUri + "/vehicles/makes")
  }

  postVehicle(vehicle: Object){
    return this.http.post(this.baseUri + "/vehicles", vehicle);
  }

  uploadImage(imageFile){
    return this.http.post(this.baseUri + "/image", imageFile);
  }
}
