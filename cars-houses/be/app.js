const express = require('express')
const app = express();
const multer = require('multer');
const mongoose = require('mongoose');
const Joi = require("joi");
const Grid = require('gridfs-stream');

const crypto = require('crypto');
const path = require('path');
const GridFsStorage = require('multer-gridfs-storage');
const methodOverride = require('method-override');
const mongoUri = "mongodb://root:password01@ds263660.mlab.com:63660/executive-test";


// You have to use this middleware for you to get a res.body
app.use(express.json());
app.use(methodOverride('_method'));

// gridfs preparation
var conn = mongoose.createConnection(mongoUri);
let gfs = undefined;
let readstream = undefined;

conn.once('open',  () => {
  // initialize stream
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection("uploads");
})

// Create storage object
const storage = new GridFsStorage({
  url: mongoUri,
  file: (req, file) => {

    return new Promise((resolve, reject) => {

      crypto.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        const filename = buf.toString('hex') + path.extname(file.originalname);
        const fileInfo = {
          filename: filename,
          bucketName: 'uploads'
        };
        resolve(fileInfo);
      });
    });
  }
});
const upload = multer({ storage });

// allow cors
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// capitalize first letter
function capitalizeFirstLetter(string){
  return string.charAt(0).toUpperCase() + string.slice(1);
}

// "schema" for a single vehicle
const vehicleSchema = new mongoose.Schema({
  identifier: String,
  make: String,
  model: String,
  bodyType: String,
  year: String,
  mileage: String,
  engineSize: String,
  price: String,
  transmission: String,
  location: String,
  condition: String,
  fuelType: String,
  color: String,
  dateAdded: {type: Date, default: Date.now}
});

// Vehicle model schema
const vehicleModelSchema = new mongoose.Schema({
  title: String
});

// Model model
const VehicleModel = mongoose.model("VehicleModel", vehicleModelSchema);

// Vehicle make schema
const vehicleMakeSchema = new mongoose.Schema({
  title: String
});

// Make model
const VehicleMake = mongoose.model("VehicleMake", vehicleMakeSchema);

// Vehicle model
const Vehicle = mongoose.model("Vehicle", vehicleSchema);

// Get all the vehicles
app.get("/vehicles", async (req, res) => {
  const vehicles = await Vehicle.find();
  res.send(vehicles);
});

// Get a vehicle by its id
app.get("/vehicles/:id", async (req, res) => {
  var id = req.params.id;
  const vehicle = await Vehicle.findById(id);
  res.send(vehicle);
});

// Get all vehicle models
app.get("/vehicles/models", async (req, res) => {
  const allVehicleModels = await VehicleModel.find();
  res.send(allVehicleModels);
});

// Get all vehicle makes
app.get("/vehicles/makes", async (req, res) => {
  const allVehicleMakes = await VehicleMake.find();
  res.send(allVehicleMakes);
});

// Create a vehicle and save it
app.post("/vehicles", upload.single('image1'), async (req, res) => {

  function makeid (){
    let text = '';
    let possible ="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 15; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  const schema = Joi.object().keys({
    make: Joi.string().alphanum().min(3).max(15).required(),
    model: Joi.string().min(1).max(15).required(),
    bodyType: Joi.string().min(3).max(128),
    year: Joi.string().length(4).required(),
    mileage: Joi.string().max(299999).min(0).required(),
    engineSize: Joi.string().length(4).required(),
    price: Joi.string().alphanum().min(6).max(9).required(),
    transmission: Joi.string().alphanum().required(),
    condition: Joi.string().min(3).max(10).alphanum(),
    fuelType: Joi.string().alphanum().min(3).max(10).required(),
    color: Joi.string().alphanum().min(3).max(10).required()

  });

  let result = Joi.validate(req.body, schema);

  if (result.error){
    res.status(400);
    res.send(result.error.details[0]["message"]);
    return;
  } else {
    let vehicle = new Vehicle({
      identifier: makeid(),
      make: req.body.make,
      model: req.body.model,
      bodyType: req.body.bodyType,
      year: req.body.year,
      mileage: req.body.mileage,
      engineSize: req.body.engineSize,
      price: req.body.price,
      transmission: req.body.transmission,
      condition: req.body.condition,
      fuelType: req.body.fuelType,
      color: req.body.color,
    });

    // create a vehicle model if it doesn't exist
    let vehicleModel = new VehicleModel({
      title: req.body.model
    });

    // create a vehicle make if it doesn't exist
    let vehicleMake = new VehicleMake({
      title: req.body.make
    });


    // save the validated vehicle 
    vehicle = await vehicle.save();

    // see if vehicle model exists
    let existsVehicleModel = await VehicleModel.find({
      title: req.body.model
    });

     // see if vehicle make exists
     let existsVehicleMake = await VehicleMake.find({
      title: req.body.make
    });

    if(existsVehicleMake.length == 0){
      vehicleMake = await vehicleMake.save()
      res.send(vehicleMake);
    } else {
      res.send(vehicleMake + " already exists");
    }

    if(existsVehicleModel.length == 0){
      vehicleModel = await vehicleModel.save()
      res.send(vehicleModel);
    } else {
      res.send(vehicleModel + " already exists");
    }

    console.log({image1: req.image1});
  }
})

// Upload image
app.post("/image", upload.single('image'), async (req, res) => {
  res.send(req.file);
  console.log(req.file);
});

// get all files
app.get('/files', (req, res) => {
  gfs.files.find().toArray((err, files) => {
    if (!files || files.length == 0) {
      return res.status(404).json({
        err: 'no files'
      });
    }
    return res.json(files);
  })
});

// get single image in json
app.get("/files/:filename", (req, res) => {
  gfs.files.findOne({filename: req.params.filename}, (err, file) => {
    if (!file || file.length === 0) {
      return res.status(404).json({
        err: "File does not exist"
      })
    }

    return res.json (file);
  })
});

// display actual image
app.get("/images/:filename", (req, res) => {
  gfs.files.findOne({filename: req.params.filename}, (err, file) => {
    if (!file || file.length === 0) {
      return res.status(404).json({
        err: "File does not exist"
      })
    }
    if (file.contentType === "image/jpeg"){
      readstream = gfs.createReadStream(file.filename);
      readstream.pipe(res);
    } else {
      res.status(403).json({
        err: "not an image"
      })
    }
  })
});

app.get('/', (req, res) => {
    res.send("hello world");
});

// server
app.listen(3000, () => {
    mongoose.connect("mongodb://root:password01@ds263660.mlab.com:63660/executive-test")
    //mongoose.connect("mongodb://10.4.115.98:27017/executive-test")
      .then(() => {
        console.log("Successfully connected to mongoDB");
      })
      .catch(err => {
        console.log(err);
      })
    console.log('App listening on port 3000!');
});
